from os import system
from src.helpers.file_ops import load_json

config = load_json('./config.json')


def run_gui():
    system(
        'python3 {}'.format(config['paths']['gui'])
    )


if __name__ == '__main__':
    run_gui()
