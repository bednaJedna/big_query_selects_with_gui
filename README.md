#How to (really)
    1. clone this repo
    2. have python 3.+  and Google Cloud SDK CLI installed and set up
    3. run _sudo python3 setup.py install (or develop)_
    4. put txt files into _./resources/sql_queries/_ dir (one file == one query)
    5. run _python3 app.py
    6. open browser and enter IP address as it is written in console/shell
    7. enter needed parameters and click button. Wait, until animation dissapers. For a big number of queries
    this may take some time, even several minutes.
    6. when finished, output data in .xlsx file can be found in _./output/_ folder