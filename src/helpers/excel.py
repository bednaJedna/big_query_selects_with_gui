"""
Handles operations with MS Excel files
- saving BQ selects output into MS Excel files
"""
import openpyxl


def save_query_output(path: str, date: str, kpi: str, output: list):
    filepath = ''.join([path, date, '_bq_results.xlsx'])

    # open xlsx file
    try:
        wb = openpyxl.load_workbook(filepath)
    except FileNotFoundError:
        openpyxl.Workbook().save(filepath)
        wb = openpyxl.load_workbook(filepath)

    # create kpi sheet
    sheet = wb.create_sheet(kpi.rstrip('.txt'))

    # fill data into xls sheet
    try:
        for x, row in enumerate(output):
            for y, value in enumerate(row):
                sheet.cell(row=x+1, column=y+1, value=value)

    except Exception as e:
        print('Error processing output data: {}'.format(str(e)))

    # save file
    wb.save(filepath)
