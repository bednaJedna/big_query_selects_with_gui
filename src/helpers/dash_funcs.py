import re


def verify_dealer(user_input: str):
    # weak, very simple verification
    # just to make sure no really wrong values are put in
    value = r"(^\d{0,3}-{1}\d{3,5}\w?$)|(^$)"

    if re.fullmatch(value, user_input):
        return user_input

    else:
        return 'Wrong Format. Try again.'


def verify_country(user_input: str):
    # weak, very simple verification
    # just to make sure no really wrong values are put in

    value = r"^\d{9}$"

    if re.fullmatch(value, user_input):
        return user_input

    else:
        return 'Wrong Format. Try again.'
