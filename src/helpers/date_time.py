"""
Handles dates and time
"""
from datetime import datetime as dt


def timestamp():
    return dt.now().strftime('%Y-%m-%d_%H:%M:%S:%f')
