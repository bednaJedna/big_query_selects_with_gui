#!/usr/bin/python3

from src.helpers.file_ops import load_json, load_file
from src.helpers.parsers import parse
from src.helpers.big_query import oneline_query, run_query, replace_select_parts
from src.helpers.excel import save_query_output

config = load_json('./config.json')


def query(inputs: list, timestamp: str):
    raw_query = load_file(''.join([config['paths']['sql_queries'], inputs[0]]))

    onelined_query = oneline_query(raw_query)
    onelined_query = replace_select_parts(tuple(inputs[1:]), onelined_query)

    run_query(onelined_query)

    raw_bq_output = load_file(config['paths']['temp'])
    bq_output = parse(raw_bq_output)

    save_query_output(config['paths']['output'], timestamp, inputs[0], bq_output)
