import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output, State
from dash.exceptions import PreventUpdate
from datetime import datetime as dt
from src.helpers.file_ops import get_file_list, load_json
from src.test.bq_script import query
from src.helpers.date_time import timestamp
from src.helpers.dash_funcs import verify_country, verify_dealer

config = load_json('./config.json')
files = get_file_list(config['paths']['sql_queries'])

external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']

gui = dash.Dash('Test GUI', external_stylesheets=external_stylesheets)
gui.scripts.config.serve_locally = True

gui.layout = html.Div(
    id='main_wrapper',
    children=[

        # storage
        dcc.Store(id='store_01', storage_type='local'),

        # main menu
        html.Div(
            id='top_menubar_wrapper',
            children=[

                # dropdown item
                html.Div(
                    id='dropdown_queries_wrapper',
                    children=[

                        html.Div('SQL Query Files to Run:'),

                        # select sql files
                        dcc.Dropdown(
                            id='dropdown_queries_list',
                            options=[
                                {'label': file.rstrip('.txt'), 'value': file} for file in files
                            ],
                            multi=True,
                            clearable=False,
                            value=[file for file in files],
                            style={
                                'width': 200
                            }

                        )
                    ],
                    style={
                        'flex-grow': '1'
                    }
                ),

                # date range picker:
                html.Div(
                    id='date_range_picker_wrapper',
                    children=[

                        html.Div('Select Start and End Date:'),

                        dcc.DatePickerRange(
                            id='date_range_picker',
                            min_date_allowed=dt(2019, 1, 1),
                            initial_visible_month=dt(2019, 1, 1)
                        )
                    ],
                    style={
                        'flex-grow': '2',
                        'width': 300
                    }
                ),

                # dealer code input
                html.Div(
                    id='dealer_code_input_wrapper',
                    children=[

                        html.Div('Enter Dealer Code'),

                        dcc.Input(
                            id='dealer_code_input',
                            placeholder='Enter the dealer code here, if needed',
                            style={
                                'width': 400
                            }
                        )
                    ],
                    style={
                        'flex-grow': '4'
                    }
                ),

                # country code input
                html.Div(
                    id='country_code_input_wrapper',
                    children=[

                        html.Div('Enter Country Code:'),

                        dcc.Input(
                            id='country_code_input',
                            placeholder='Enter the country BQ code, if needed',
                            style={
                                'width': 400
                            }
                        )
                    ],
                    style={
                        'flex-grow': '4'
                    }
                ),

                # start query button
                html.Div(
                    id='start_querying_btt_wrapper',
                    children=[
                        html.Button(
                            id='bttn_start_querying',
                            children='Run BQ!',
                            n_clicks=0
                        )
                    ],
                    style={
                        'flex-grow': '1'
                    }
                )
            ],
            style={
                'display': 'flex',
                'flex-direction': 'row',
                'flex-wrap': 'wrap',
                'justify-content': 'space-around',
                'align-items': 'flex-end',
                'align-content': 'space-between'
            }
        ),

        # main 1 for daq
        html.Div(
            id='Main 1 Wrapper',
            children=[

                # progress bar
                dcc.Loading(
                    id='load_notifier',
                    children=[

                        # element to render
                        html.Div(
                            id='load_output'
                        )
                    ],
                    type='cube',
                    fullscreen=True
                )
            ]
        )
    ]
)


# [[[[[[[[[[[CALLBACKS]]]]]]]]]]]]]]]]]]]]]
@gui.callback(
    Output('store_01', 'data'),
    [Input('bttn_start_querying', 'n_clicks')],
    [State('dropdown_queries_list', 'value'),
     State('date_range_picker', 'start_date'),
     State('date_range_picker', 'end_date'),
     State('dealer_code_input', 'value'),
     State('country_code_input', 'value')]
)
def start_querying(click, queries, start_date, end_date, dealer_code, country_code):
    if queries and start_date and end_date and dealer_code and country_code:
        if click > 0:
            return [queries, start_date, end_date, dealer_code, country_code]

        else:
            PreventUpdate()


# loop callbacks for each of the sql query file
@gui.callback(
    Output('load_output', 'children'),
    [Input('store_01', 'data')]
)
def run_query(data):
    tstamp = timestamp()

    for file in files:
        if data is None:
            PreventUpdate()
            break

        else:
            output = list()
            for x, each in enumerate(data):
                if isinstance(each, list) and (file in each):
                    output.append(file)

                elif x in [1, 2]:
                    output.append(each.replace('-', '').strip())

                else:
                    output.append(each.strip())

            query(output, tstamp)


if __name__ == '__main__':
    gui.run_server(debug=False)
